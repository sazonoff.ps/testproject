// Fill out your copyright notice in the Description page of Project Settings.


#include "TestProject/Public/Interfaces/InspectInterface.h"

// Add default functionality here for any IInspectInterface functions that are not pure virtual.
void IInspectInterface::Interact(ATestProjectCharacter* Character)
{
	
}

void IInspectInterface::Inspect(ATestProjectCharacter* Character, UStaticMesh* Item, FName ItemName,
                                FText ItemDescription, bool bIsUI, UUserWidget* UI)
{
}
