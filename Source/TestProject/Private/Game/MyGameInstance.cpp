// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/MyGameInstance.h"

bool UMyGameInstance::GetItemInfoByName(const FName InItemName, FItemsInfo& OutInfo) const
{
	bool bIsFind = false;
	
	if (!ItemInfoTable) return false;
	
	TArray<FName>RowNames = ItemInfoTable->GetRowNames();
		
	int8 i = 0;
	while (i < RowNames.Num() && !bIsFind)
	{
		const FItemsInfo* ItemInfoRow = ItemInfoTable->FindRow<FItemsInfo>(RowNames[i], "");
		if (ItemInfoRow->ItemName == InItemName)
		{
			OutInfo = (*ItemInfoRow);	
			bIsFind = true;
		}
		i++;
	}
	return bIsFind;
}
