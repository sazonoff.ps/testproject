// Copyright Epic Games, Inc. All Rights Reserved.

#include "Game/TestProjectGameMode.h"
#include "UI/TestProjectHUD.h"
#include "Character/TestProjectCharacter.h"

ATestProjectGameMode::ATestProjectGameMode()
	: Super()
{
	DefaultPawnClass = ATestProjectCharacter::StaticClass();
	HUDClass = ATestProjectHUD::StaticClass();
}
