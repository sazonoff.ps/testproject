// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InventorySlotWidget.h"
#include "UI/InventoryActionWidget.h"
#include "UI/DragAndDropWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Border.h"
#include "Inventory/InventoryComponent.h"
#include "UI/InventoryDragDropOperation.h"


FSlateBrush UInventorySlotWidget::GetItemIcon() const
{
	FSlateBrush SlateBrush = UWidgetBlueprintLibrary::MakeBrushFromTexture(ItemInfo.ItemIcon);
	return SlateBrush;
}

FReply UInventorySlotWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	FReply Reply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);

	if (InMouseEvent.GetEffectingButton() == EKeys::LeftMouseButton)
	{
		if (ActionWidget)
		{
			ActionWidget->RemoveFromParent();
			ActionWidget = nullptr;
			OnToggleOpacity.Broadcast(true);
		}
		Reply.DetectDrag(TakeWidget(), EKeys::LeftMouseButton);
		return Reply;
	}
	if (InMouseEvent.IsMouseButtonDown(EKeys::RightMouseButton))
	 {
		ToggleActions();
	 	return FReply::Handled();
	 }
	return Reply;
}

void UInventorySlotWidget::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);
	if (SlotBorder && !ItemInfo.ItemName.IsNone())
	{
		SlotBorder->SetBrushColor(ActivatedBorder);
	}
}

void UInventorySlotWidget::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseLeave(InMouseEvent);
	if (SlotBorder)
	{
		SlotBorder->SetBrushColor(DeactivatedBorder);
	}
}

void UInventorySlotWidget::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent,
	UDragDropOperation*& OutOperation)
{
	if (ItemInfo.ItemName.IsNone() || !DragAndDropWidgetClass) return;
	
	UDragAndDropWidget* DragWidget = CreateWidget<UDragAndDropWidget>(GetWorld(), DragAndDropWidgetClass);

	if (DragWidget)
	{
		DragWidget->ItemInfo = ItemInfo;
		UInventoryDragDropOperation* DragDropOperation = NewObject<UInventoryDragDropOperation>();

		if (DragDropOperation)
		{
			DragDropOperation->ItemInfo = ItemInfo;
			DragDropOperation->Inventory = InventoryComponent;
			DragDropOperation->ItemSlotIndex = SlotIndex;
			DragDropOperation->DefaultDragVisual = DragWidget;
			DragDropOperation->Pivot = EDragPivot::CenterCenter;
			OutOperation = DragDropOperation;
		}
	}
}

bool UInventorySlotWidget::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
	UDragDropOperation* InOperation)
{
	const UInventoryDragDropOperation* DragDropOperation = Cast<UInventoryDragDropOperation>(InOperation);
	if (DragDropOperation)
	{
		 if (ItemInfo.ItemName.IsNone())
		 {
		 	const FItemsInfo EmptyInfo;
		 	DragDropOperation->Inventory->UpdateSlotInfo(DragDropOperation->ItemSlotIndex, EmptyInfo);
		 }
		 else
		 {
		 	DragDropOperation->Inventory->UpdateSlotInfo(DragDropOperation->ItemSlotIndex, ItemInfo);
		 }
		
		ItemInfo = DragDropOperation->ItemInfo;
		OnChangeSlotInfo.Broadcast(SlotIndex, ItemInfo);
		OnRefreshSlot.Broadcast();
		return true;
	}
	
	return false;
}

void UInventorySlotWidget::NativeOnDragEnter(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
	UDragDropOperation* InOperation)
{
	Super::NativeOnDragEnter(InGeometry, InDragDropEvent, InOperation);

	
	const UInventoryDragDropOperation* DragDropOperation = Cast<UInventoryDragDropOperation>(InOperation);

	if (DragDropOperation && SlotBorder)
	{
		SlotBorder->SetBrushColor(ActivatedBorder);
	}
}

void UInventorySlotWidget::NativeOnDragLeave(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDragLeave(InDragDropEvent, InOperation);

	const UInventoryDragDropOperation* DragDropOperation = Cast<UInventoryDragDropOperation>(InOperation);

	if (DragDropOperation && SlotBorder)
	{
		SlotBorder->SetBrushColor(DeactivatedBorder);
	}
}

void UInventorySlotWidget::ToggleActions()
{
	if (ItemInfo.ItemName.IsNone()) return;
	
	if (!ActionWidget)
	{
		OnToggleOpacity.Broadcast(false);
		ActionWidget = CreateWidget<UInventoryActionWidget>(GetWorld(), InventoryActionClass);
		
		if (ActionWidget)
		{
			ActionWidget->ItemInfo = ItemInfo;
			ActionWidget->InventorySlotWidget = this;
			UpdateOpacity(true);
			OnAddActions.Broadcast(ActionWidget);
		}
	}
	else
	{
		ActionWidget->RemoveFromParent();
		ActionWidget = nullptr;
		OnToggleOpacity.Broadcast(true);
	}
}


void UInventorySlotWidget::UpdateOpacity(const bool bActivated)
{
	float OpacityValue;
	bActivated ? OpacityValue = 1.0f : OpacityValue = 0.6f;
	SetRenderOpacity(OpacityValue);
}

UInventoryActionWidget* UInventorySlotWidget::GetActionWidget()
{
	return ActionWidget;
}

void UInventorySlotWidget::SetActionWidget(UInventoryActionWidget* NewActionWidget)
{
	ActionWidget = NewActionWidget;
}