// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/DragAndDropWidget.h"
#include "Components/Image.h"



void UDragAndDropWidget::NativePreConstruct()
{
	Super::NativePreConstruct();

	DragImage->SetBrushFromTexture(ItemInfo.ItemIcon);
}