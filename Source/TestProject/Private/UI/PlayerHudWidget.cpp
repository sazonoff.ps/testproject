// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PlayerHudWidget.h"

#include "Blueprint/WidgetLayoutLibrary.h"
#include "Character/TestProjectCharacter.h"
#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/GridPanel.h"
#include "Inventory/InventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UI/InventoryActionWidget.h"
#include "UI/InventorySlotWidget.h"
#include "UI/InventoryWindowWidget.h"

void UPlayerHudWidget::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerCharacter = Cast<ATestProjectCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(),0));
	if (InventoryComponent)
	{
		InventoryComponent->OnInventoryOpen.AddUObject(this, &UPlayerHudWidget::CreateInventoryWindow);
	}
	
}

void UPlayerHudWidget::CreateInventoryWindow(FName InName)
{
	APlayerController* OwnerPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (!OwnerPlayerController) return;
	
	if (!InventoryWindow)
	{
		InventoryWindow = CreateWidget<UInventoryWindowWidget>(GetWorld(), InventoryWindowClass);
		if (InventoryComponent && InventoryWindow && MainCanvas)
		{
			if (PlayerCharacter)
			{
				InventoryWindow->Character = PlayerCharacter;
			}
			InventoryWindow->SetInventoryComponent(InventoryComponent);
			InventoryWindow->OnSlotCreated.AddUObject(this, &UPlayerHudWidget::ActionToSlot);
			InventoryWindow->InventoryName = InventoryComponent->InventoryName;
			UCanvasPanelSlot* CanvasPanel = MainCanvas->AddChildToCanvas(InventoryWindow);
			
			if (CanvasPanel)
			{
				CanvasPanel->SetAutoSize(true);
				FAnchors Anchors;
				Anchors.Minimum = AnchorsMin;
				Anchors.Maximum = AnchorsMax;
				FVector2D Result;
				GEngine->GameViewport->GetViewportSize(Result);
				CanvasPanel->SetAnchors(Anchors);
				CanvasPanel->SetAlignment(Alignment);
				
				CanvasPanel->SetPosition(InventoryComponent->InventoryWidgetPosition);
			}
			
			FInputModeUIOnly InputModeDataUIOnly;
			InputModeDataUIOnly.SetWidgetToFocus(InventoryWindow->TakeWidget());
			InputModeDataUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			OwnerPlayerController->SetInputMode(InputModeDataUIOnly);
			OwnerPlayerController->SetShowMouseCursor(true);
		}
	}
	else
	{
		const UInventorySlotWidget* InventSlot = Cast<UInventorySlotWidget>(
			InventoryWindow->GetInventoryGrid()->GetChildAt(0));
		if (InventSlot)
		{
			const TArray<UWidget*> GridChild = InventoryWindow->GetInventoryGrid()->GetAllChildren();
			if (GridChild.Num() != 0)
			{
				for (int32 i = 0; i < GridChild.Num(); i++)
				{
					UInventorySlotWidget* CurrentSlot = Cast<UInventorySlotWidget>(GridChild[i]);
					if (CurrentSlot)
					{
						if (CurrentSlot->GetActionWidget())
						{
							CurrentSlot->GetActionWidget()->RemoveFromParent();
							CurrentSlot->SetActionWidget(nullptr);
						}
					}
				}
			}
		}
		InventoryWindow->RemoveFromParent();
		InventoryWindow = nullptr;
		OwnerPlayerController->SetInputMode(FInputModeGameOnly());
		OwnerPlayerController->SetShowMouseCursor(false);
	}
}

void UPlayerHudWidget::ActionToSlot()
{
	if (InventoryWindow && InventoryWindow->GetSlotWidget())
	{
		InventoryWindow->GetSlotWidget()->OnAddActions.AddUObject(this, &UPlayerHudWidget::AddActionWidget);
	}
}

void UPlayerHudWidget::AddActionWidget(UWidget* NewWidget)
{
	if (NewWidget)
	{
		UCanvasPanelSlot* CanvasPanel = MainCanvas->AddChildToCanvas(NewWidget);

		if (CanvasPanel)
		{
			CanvasPanel->SetAutoSize(true);
			if (GetWorld())
			{
				float XPosition;
				float YPosition;
				UWidgetLayoutLibrary::GetMousePositionScaledByDPI(UGameplayStatics::GetPlayerController(GetWorld(), 0),
				                                                  XPosition, YPosition);
				CanvasPanel->SetPosition(FVector2D(XPosition,YPosition));
			}
		}
	}
}
