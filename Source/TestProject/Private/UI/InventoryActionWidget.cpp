// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InventoryActionWidget.h"

#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Character/TestProjectCharacter.h"
#include "Components/Button.h"
#include "Inventory/InventoryComponent.h"
#include "Items/ItemInspectViewer.h"
#include "Kismet/GameplayStatics.h"
#include "UI/InventorySlotWidget.h"
#include "UI/InventoryWindowWidget.h"

void UInventoryActionWidget::NativeConstruct()
{
	Super::NativeConstruct();

	PlayerCharacter = Cast<ATestProjectCharacter>(GetOwningPlayerPawn());

	if (UseButton)
	{
		UseButton->OnClicked.AddDynamic(this, &UInventoryActionWidget::UseItem);
	}
	if (InfoButton)
	{
		InfoButton->OnClicked.AddDynamic(this, &UInventoryActionWidget::OpenInspector);
	}
	if (DropButton)
	{
		DropButton->OnClicked.AddDynamic(this, &UInventoryActionWidget::DropItem);
	}
}


void UInventoryActionWidget::OpenInspector()
{
	APlayerController* OwnerPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (OwnerPlayerController)
	{
		OwnerPlayerController->SetInputMode(FInputModeGameOnly());
		OwnerPlayerController->SetShowMouseCursor(false);
	}

	FActorSpawnParameters SpawnParams;
	FTransform Offset = FTransform();
	Offset.SetLocation(FVector(1000000000.0f, 0.0f, 0.0f));
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	AItemInspectViewer* InspectViewer = GetWorld()->SpawnActor<AItemInspectViewer>(
		InspectorClass, Offset, SpawnParams);

	TArray<UUserWidget*> FoundWidgets;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(GetWorld(), FoundWidgets, UInventoryWindowWidget::StaticClass(), false);
	
	if (!InspectViewer || !PlayerCharacter) return;
	Cast<IInspectInterface>(InspectViewer)->Inspect(PlayerCharacter, ItemInfo.ItemMesh, ItemInfo.ItemName,
													ItemInfo.ItemDescription, true, FoundWidgets[0]);
}

void UInventoryActionWidget::UseItem()
{
	if (PlayerCharacter && PlayerCharacter->GetInventoryComponent() && InventorySlotWidget)
	{
		PlayerCharacter->GetInventoryComponent()->RemoveItem(InventorySlotWidget->SlotIndex);
		InventorySlotWidget->OnRefreshSlot.Broadcast();
		
	}
	
	RemoveFromParent();
}

void UInventoryActionWidget::DropItem()
{
	if (PlayerCharacter && PlayerCharacter->GetInventoryComponent())
	{
		PlayerCharacter->GetInventoryComponent()->RemoveItem(InventorySlotWidget->SlotIndex);
	}
}
