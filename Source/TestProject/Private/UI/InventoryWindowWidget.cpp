// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InventoryWindowWidget.h"

#include "Character/TestProjectCharacter.h"
#include "Components/GridPanel.h"
#include "Components/GridSlot.h"
#include "UI/InventorySlotWidget.h"
#include "Inventory/InventoryComponent.h"
#include "Items/CrimeBoard.h"
#include "UI/InventoryActionWidget.h"


void UInventoryWindowWidget::NativeConstruct()
{
	Super::NativeConstruct();

	bIsFocusable = true;
	SetKeyboardFocus();
	CreateInventorySlots();
	
	if (InventoryComponent)
	{
		InventoryComponent->OnInventoryUpdate.AddUObject(this, &UInventoryWindowWidget::RefreshSlot);
	}
	
}

FReply UInventoryWindowWidget::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
	const FKey Key = InKeyEvent.GetKey();
	KeyInputs(Key.GetFName());
	return FReply::Handled();
}


UGridPanel* UInventoryWindowWidget::GetInventoryGrid() const
{
	return InventoryGrid;
}

UInventorySlotWidget* UInventoryWindowWidget::GetSlotWidget() const
{
	return InventorySlot;
}

void UInventoryWindowWidget::SetInventoryComponent(UInventoryComponent* InInventory)
{
	InventoryComponent = InInventory;
}

void UInventoryWindowWidget::CreateInventorySlots()
{
	if (!InventoryComponent) return;
	
	for (int32 i = 0; i < InventoryComponent->SlotsCount; i++)
	{
		InventorySlot = CreateWidget<UInventorySlotWidget>(this, InventorySlotWidgetClass);
		if (InventorySlot)
		{
			InventorySlot->SlotIndex = i;
			InventorySlot->ItemInfo = InventoryComponent->Inventory[i];
			InventorySlot->InventoryComponent = InventoryComponent;
			InventorySlot->Character = Character;
			InventorySlot->OnToggleOpacity.AddUObject(this, &UInventoryWindowWidget::ToggleOpacity);
			UGridSlot* GridSlot = InventoryGrid->AddChildToGrid(InventorySlot);
			if (GridSlot)
			{
				GridSlot->SetColumn(i % 4);
				GridSlot->SetRow(i / 4);
			}
			OnSlotCreated.Broadcast();
			InventorySlot->OnRefreshSlot.AddUObject(this, &UInventoryWindowWidget::RefreshSlot);
			InventorySlot->OnChangeSlotInfo.AddUObject(this, &UInventoryWindowWidget::ChangeSlotInfo);
			InventorySlot->OnTransferItem.AddUObject(this, &UInventoryWindowWidget::Transfer);
		}
	}

	if (InventoryComponent->GetOwner() != Character)
	{
		if (InventoryGrid)
		{
			const TArray<UWidget*> GridChild = InventoryGrid->GetAllChildren();
			if (GridChild.Num() != 0)
			{
				for (int32 i = 0; i < GridChild.Num(); i++)
				{
					if (GridSlotTranslation.IsValidIndex(i))
					{
						GridChild[i]->SetRenderTranslation(GridSlotTranslation[i]);
					}
				}
			}
		}
	}
}

void UInventoryWindowWidget::ToggleOpacity(const bool bActivated)
{
	if (InventoryGrid)
	{
		const TArray<UWidget*> GridChild = InventoryGrid->GetAllChildren();
		if (GridChild.Num() != 0)
		{
			for (int32 i = 0; i < GridChild.Num(); i++)
			{
				UInventorySlotWidget* InventSlot = Cast<UInventorySlotWidget>(GridChild[i]);
				if (InventSlot)
				{
					if (InventSlot->GetActionWidget())
					{
						InventSlot->GetActionWidget()->RemoveFromParent();
						InventSlot->SetActionWidget(nullptr);
					}
					InventSlot->UpdateOpacity(bActivated);
				}
			}
		}
	}
}

void UInventoryWindowWidget::RefreshSlot()
{
	if (InventoryGrid && InventoryComponent)
	{
		const TArray<UWidget*> GridChild = InventoryGrid->GetAllChildren();
		if (GridChild.Num() != 0)
		{
			for (int32 i = 0; i < GridChild.Num(); i++)
			{
				UInventorySlotWidget* InventSlot = Cast<UInventorySlotWidget>(GridChild[i]);
				if (InventSlot)
				{
					InventSlot->ItemInfo = InventoryComponent->Inventory[i];
					ToggleOpacity(true);
					SetKeyboardFocus();
				}
			}
		}
	}
}

void UInventoryWindowWidget::ChangeSlotInfo(const int32 IndexSlot, const FItemsInfo NewInfo)
{
	if (InventoryComponent)
	{
		InventoryComponent->UpdateSlotInfo(IndexSlot, NewInfo);
	}
}

void UInventoryWindowWidget::KeyInputs(const FName KeyName)
{
	if (KeyName == TEXT("Tab") || KeyName == TEXT("E"))
	{
		if (Character && Character->GetInventoryComponent())
		{
			Character->GetInventoryComponent()->ToggleInventory();
			const ACrimeBoard* CrimeBoardLocal = Character->GetInventoryComponent()->CrimeBoard;
			if (CrimeBoardLocal)
			{
				CrimeBoardLocal->GetInventoryComponent()->ToggleInventory();
				Character->GetInventoryComponent()->CrimeBoard = nullptr;
			}
		}
	}
}

void UInventoryWindowWidget::Transfer(const int32 IndexSlot, const FItemsInfo NewInfo, const FName InventName)
{
	if (InventoryComponent)
	{
		if (InventoryComponent->GetOwner() == Character)
		{
			InventoryComponent->TransferItem(IndexSlot, NewInfo, InventoryName);
		}
		else
		{
			if (Character->GetInventoryComponent()->AddToInventory(NewInfo))
			{
				InventoryComponent->RemoveItem(IndexSlot);
			}
		}
	}
	
}