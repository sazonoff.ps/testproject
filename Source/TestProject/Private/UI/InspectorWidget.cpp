// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/InspectorWidget.h"

#include "Components/ScrollBox.h"
#include "Components/TextBlock.h"
#include "Kismet/KismetMathLibrary.h"

void UInspectorWidget::NativeConstruct()
{
	Super::NativeConstruct();

	ItemDescriptionBlock->SetText(ItemDescription);
	if (!GetWorld()) return;
	GetWorld()->GetTimerManager().SetTimer(StartScrollTimerHandle, this, &UInspectorWidget::StartScroll,
										   2.0f, false);
}

void UInspectorWidget::StartScroll()
{
	if (!GetWorld()) return;
	GetWorld()->GetTimerManager().ClearTimer(StartScrollTimerHandle);
	ScrollBox->SetScrollOffset(0.0f);
	GetWorld()->GetTimerManager().SetTimer(ScrollTimerHandle, this, &UInspectorWidget::Scroll,
										   0.035f, true);
	
}

void UInspectorWidget::Scroll()
{
	ScrollBox->SetScrollOffset(ScrollBox->GetScrollOffset() + 1.0f);
	const float NormalizeValue = UKismetMathLibrary::NormalizeToRange(ScrollBox->GetScrollOffset() + 1.0f, 0.0f,
	                                                                  ScrollBox->GetScrollOffsetOfEnd());
	if (NormalizeValue >= 1.0f)
	{
		StopScroll();
	}
}

void UInspectorWidget::StopScroll()
{
	if (!GetWorld()) return;
	
	GetWorld()->GetTimerManager().ClearTimer(ScrollTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(StartScrollTimerHandle, this, &UInspectorWidget::StartScroll,
										   2.0f, false);
}
