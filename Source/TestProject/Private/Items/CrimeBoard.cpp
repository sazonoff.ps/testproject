// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/CrimeBoard.h"
#include "Character/TestProjectCharacter.h"
#include "Inventory/InventoryComponent.h"


ACrimeBoard::ACrimeBoard()
{
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
}

UInventoryComponent* ACrimeBoard::GetInventoryComponent() const
{
	return InventoryComponent;
}

// Called when the game starts or when spawned
void ACrimeBoard::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACrimeBoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACrimeBoard::Interact(ATestProjectCharacter* Character)
{
	if (Character)
	{
		Character->GetInventoryComponent()->CrimeBoard = this;
		Character->GetInventoryComponent()->ToggleInventory();
		InventoryComponent->ToggleInventory();
	}
}

