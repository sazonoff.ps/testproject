// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/ItemInspectViewer.h"

#include "Character/TestProjectCharacter.h"
#include "Components/SceneCaptureComponent2D.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UI/InspectorWidget.h"

// Sets default values
AItemInspectViewer::AItemInspectViewer()
{
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);
	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	ItemMesh->SetupAttachment(RootComponent);
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	SceneCaptureComponent = CreateDefaultSubobject<USceneCaptureComponent2D>(TEXT("SceneCapture"));
	SceneCaptureComponent->SetRelativeLocation(FVector(-60.0f, 0.0f, 10.0f));
	SceneCaptureComponent->SetupAttachment(CameraBoom);
}


// Called when the game starts or when spawned
void AItemInspectViewer::BeginPlay()
{
	Super::BeginPlay();
}

void AItemInspectViewer::Inspect(ATestProjectCharacter* Character, UStaticMesh* Item, FName ItemName,
	FText ItemDescription, bool bIsUI, UUserWidget* UI)
{
	if (Character && Item)
	{
		ItemMesh->SetStaticMesh(Item);
		PlayerCharacter = Character;
		bUseUI = bIsUI;
		InventoryUI = UI;
		InspectWidget = CreateWidget<UUserWidget>(GetWorld(), InteractWidgetClass);
		if (InspectWidget)
		{
			InspectWidget->AddToViewport();
		}
		PlayerCharacter->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		BindToInput();
	}
}

void AItemInspectViewer::BindToInput()
{
	InputComponent = NewObject<UInputComponent>(this);
	InputComponent->RegisterComponent();
	if (InputComponent)
	{
		InputComponent->BindAction("Interact", IE_Pressed, this, &AItemInspectViewer::StartInteract);
		InputComponent->BindAction("RotateItem", IE_Pressed, this, &AItemInspectViewer::StartRotateItem);
		InputComponent->BindAction("RotateItem", IE_Released, this, &AItemInspectViewer::StopRotateItem);
		InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AItemInspectViewer::ZoomIn);
		InputComponent->BindAction("ZoomOut", IE_Pressed, this, &AItemInspectViewer::ZoomOut);
		InputComponent->BindAxis("Turn", this,&AItemInspectViewer::RotateXAxis);
		InputComponent->BindAxis("LookUp", this,&AItemInspectViewer::RotateYAxis);
		EnableInput(GetWorld()->GetFirstPlayerController());
	}
}

void AItemInspectViewer::StartInteract()
{
	
	DisableInput(GetWorld()->GetFirstPlayerController());
	if (PlayerCharacter)
	{
		PlayerCharacter->EnableInput(GetWorld()->GetFirstPlayerController());
	}
	if (InspectWidget)
	{
	    InspectWidget->RemoveFromParent();
        InspectWidget = nullptr;
	}
	
	Destroy();
	
	if (bUseUI)
	{
		if (InventoryUI)
		{
			FInputModeUIOnly InputModeDataUIOnly;
			InputModeDataUIOnly.SetWidgetToFocus(InventoryUI->TakeWidget());
			InputModeDataUIOnly.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
			UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetInputMode(InputModeDataUIOnly);
			UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetShowMouseCursor(true);
			InventoryUI->SetKeyboardFocus();
		}
	}
}

void AItemInspectViewer::StartRotateItem()
{
	bRotated = true;
}

void AItemInspectViewer::StopRotateItem()
{
	bRotated = false;
}

void AItemInspectViewer::RotateXAxis(const float Value)
{
	if (bRotated)
	{
		ItemMesh->SetWorldRotation(
			FRotator(FQuat(ItemMesh->GetComponentRotation()) * FQuat(FRotator(0.0f, Value * (-5.0f), 0.0f))));
	}
}

void AItemInspectViewer::RotateYAxis(const float Value)
{
	if (bRotated)
	{
		ItemMesh->SetWorldRotation(
			FRotator(FQuat(ItemMesh->GetComponentRotation()) * FQuat(FRotator(Value * (-5.0f), 0.0f, 0.0f))));
	}
}

void AItemInspectViewer::ZoomIn()
{
	SceneCaptureComponent->FOVAngle = FMath::Clamp(SceneCaptureComponent->FOVAngle - ZoomSpeed, 47.0f, 105.0f);
}

void AItemInspectViewer::ZoomOut()
{
	SceneCaptureComponent->FOVAngle = FMath::Clamp(SceneCaptureComponent->FOVAngle + ZoomSpeed, 47.0f, 105.0f);
}


void AItemInspectViewer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

