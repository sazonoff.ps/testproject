// Fill out your copyright notice in the Description page of Project Settings.


#include "Items/ItemMaster.h"

#include "Character/TestProjectCharacter.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Game/MyGameInstance.h"
#include "Inventory/InventoryComponent.h"
#include "Items/ItemInspectViewer.h"
#include "Kismet/GameplayStatics.h"
#include "UI/InteractWidget.h"

// Sets default values
AItemMaster::AItemMaster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(RootComponent);
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSphereRadius(100.0f);
	CollisionSphere->SetupAttachment(StaticMesh);
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	WidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	WidgetComponent->SetDrawSize(FVector2D(50.0f, 50.0f));
	WidgetComponent->SetupAttachment(StaticMesh);

}

// Called when the game starts or when spawned
void AItemMaster::BeginPlay()
{
	Super::BeginPlay();
	InitItem();
}

void AItemMaster::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	
	if (OtherActor)
	{
		PlayerCharacter = Cast<ATestProjectCharacter>(OtherActor);
		if (!PlayerCharacter) return;
		PlayerCharacter->LookAtActor = this;
		InteractWidget = CreateWidget<UUserWidget>(GetWorld(), InteractWidgetClass);
		if (InteractWidget)
		{
			WidgetComponent->SetWidget(InteractWidget);
			WidgetComponent->SetVisibility(true);
		}
	}
}

void AItemMaster::NotifyActorEndOverlap(AActor* OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);

	if (OtherActor)
	{
		PlayerCharacter = Cast<ATestProjectCharacter>(OtherActor);
		if (!PlayerCharacter || !InteractWidget) return;
		PlayerCharacter->LookAtActor = nullptr;
		InteractWidget->RemoveFromParent();
		InteractWidget = nullptr;
		WidgetComponent->SetVisibility(false);
	}
}

void AItemMaster::InitItem()
{
	if (!GetWorld() || ItemName.IsNone()) return;

	const UMyGameInstance* GameInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
	
	if (!GameInstance) return;
	
	FItemsInfo NewItemInfo;
	if (GameInstance->GetItemInfoByName(ItemName,NewItemInfo))
	{
		ItemInfo = NewItemInfo;
		if (ItemInfo.ItemMesh)
		{
			StaticMesh->SetStaticMesh(ItemInfo.ItemMesh);
		}
	}	
}

// Called every frame
void AItemMaster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}



void AItemMaster::Interact(ATestProjectCharacter* Character)
{
	if (!Character) return;

	FActorSpawnParameters SpawnParams;
	FTransform Offset = FTransform();
	Offset.SetLocation(FVector(100000000.0f, 0.0f, 0.0f));
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	AItemInspectViewer* InspectViewer = GetWorld()->SpawnActor<AItemInspectViewer>(
		InspectorClass, Offset, SpawnParams);
		
	if (!InspectViewer) return;
		
	Cast<IInspectInterface>(InspectViewer)->Inspect(Character, StaticMesh->GetStaticMesh(), ItemInfo.ItemName,
													ItemInfo.ItemDescription, false, nullptr);
	
	UInventoryComponent* InventoryComponent = Cast<UInventoryComponent>(
		Character->FindComponentByClass(UInventoryComponent::StaticClass()));
	
	if (InventoryComponent && InventoryComponent->AddToInventory(ItemInfo))
	{
		Destroy();
	}
}
