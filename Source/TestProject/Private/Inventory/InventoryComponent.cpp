// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/InventoryComponent.h"

#include "Items/CrimeBoard.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	Inventory.SetNum(SlotsCount);
	
}

void UInventoryComponent::ToggleInventory()
{
	OnInventoryOpen.Broadcast(InventoryName);
}

bool UInventoryComponent::AddToInventory(const FItemsInfo NewItem)
{
	int32 SlotIndex;
	if (!CheckCanTakeItem(SlotIndex) || !Inventory.IsValidIndex(SlotIndex)) return false;
	
	Inventory[SlotIndex] = NewItem;
	OnInventoryUpdate.Broadcast();
	return true;
}


bool UInventoryComponent::CheckCanTakeItem(int32& OutFreeSlot) const
{
	for (int8 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i].ItemName.IsNone())
		{
			OutFreeSlot = i;
			return true;
		}
	}
	return false;
}

void UInventoryComponent::RemoveItem(const int32 SlotIndex)
{
	if (Inventory.IsValidIndex(SlotIndex))
	{
		const FItemsInfo EmptySlot;
		Inventory[SlotIndex] = EmptySlot;
		OnInventoryUpdate.Broadcast();
	}
}

void UInventoryComponent::TransferItem(const int32 SlotIndex, const FItemsInfo ItemInfo, const FName InventName)
{
	if (!ItemInfo.ItemName.IsNone())
	{
		if (CrimeBoard)
		{
			CrimeBoard->GetInventoryComponent()->AddToInventory(ItemInfo);
			RemoveItem(SlotIndex);
			OnInventoryUpdate.Broadcast();
		}
	}
}

void UInventoryComponent::UpdateSlotInfo(const int32 IndexSlot, const FItemsInfo NewInfo)
{
	if (Inventory.IsValidIndex(IndexSlot))
	{
		Inventory[IndexSlot].ItemClass = NewInfo.ItemClass;
		Inventory[IndexSlot].ItemDescription = NewInfo.ItemDescription;
		Inventory[IndexSlot].ItemIcon = NewInfo.ItemIcon;
		Inventory[IndexSlot].ItemMesh = NewInfo.ItemMesh;
		Inventory[IndexSlot].ItemName = NewInfo.ItemName;
		OnInventoryUpdate.Broadcast();
	}
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

