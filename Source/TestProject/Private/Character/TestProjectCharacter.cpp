// Copyright Epic Games, Inc. All Rights Reserved.

#include "Character/TestProjectCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Inventory/InventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Interfaces/InspectInterface.h"


ATestProjectCharacter::ATestProjectCharacter()
{
	
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
	
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f));
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
}

UInventoryComponent* ATestProjectCharacter::GetInventoryComponent() const
{
	return InventoryComponent;
}

void ATestProjectCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ATestProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	
	check(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis("MoveForward", this, &ATestProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATestProjectCharacter::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATestProjectCharacter::StartInteract);
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, InventoryComponent, &UInventoryComponent::ToggleInventory);

}

void ATestProjectCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}


void ATestProjectCharacter::MoveForward(const float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATestProjectCharacter::MoveRight(const float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}



void ATestProjectCharacter::StartInteract()
{
	if (LookAtActor)
	{
		Cast<IInspectInterface>(LookAtActor)->Interact(this);
	}
}


