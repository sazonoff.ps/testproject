// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FunctionLibrary/Types.h"
#include "DragAndDropWidget.generated.h"


class UImage;
/**
 * 
 */
UCLASS()
class TESTPROJECT_API UDragAndDropWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UImage* DragImage;
	UPROPERTY(BlueprintReadWrite, Meta=(ExposeOnSpawn=true))
	FItemsInfo ItemInfo;
	

protected:

	virtual void NativePreConstruct() override;
};
