// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/DragDropOperation.h"
#include "FunctionLibrary/Types.h"
#include "InventoryDragDropOperation.generated.h"

class UInventoryComponent;
/**
 * 
 */
UCLASS()
class TESTPROJECT_API UInventoryDragDropOperation : public UDragDropOperation
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FItemsInfo ItemInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ItemSlotIndex;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UInventoryComponent* Inventory = nullptr;
};
