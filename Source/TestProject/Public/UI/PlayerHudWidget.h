// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PlayerHudWidget.generated.h"

class ATestProjectCharacter;
class ACrimeBoard;
class UCanvasPanel;
class UInventoryWindowWidget;
class UInventoryComponent;
/**
 * 
 */
UCLASS()
class TESTPROJECT_API UPlayerHudWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY()
	UInventoryWindowWidget* InventoryWindow = nullptr;
	UPROPERTY()
	ATestProjectCharacter* PlayerCharacter = nullptr;
	
public:
	UPROPERTY(BlueprintReadWrite, Category = "Inventory", Meta=(ExposeOnSpawn=true))
	UInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UCanvasPanel* MainCanvas = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UInventoryWindowWidget> InventoryWindowClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D AnchorsMin = FVector2D(1.0f, 0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D AnchorsMax = FVector2D(1.0f, 0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D Alignment = FVector2D(0.0f, 0.0f);

protected:
	virtual void NativeConstruct() override;
	UFUNCTION(BlueprintCallable)
	void CreateInventoryWindow(FName InName);
	UFUNCTION(BlueprintCallable)
	void ActionToSlot();
	UFUNCTION(BlueprintCallable)
	void AddActionWidget(UWidget* NewWidget);
};
