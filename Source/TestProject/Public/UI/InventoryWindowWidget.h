// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "FunctionLibrary/Types.h"
#include "InventoryWindowWidget.generated.h"

class ATestProjectCharacter;
class UGridPanel;
class UInventorySlotWidget;
class UInventoryComponent;

DECLARE_MULTICAST_DELEGATE(FOnSlotCreatedSignature);

UCLASS()
class TESTPROJECT_API UInventoryWindowWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY()
	UInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY()
	UInventorySlotWidget* InventorySlot = nullptr;
	
protected:

	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UGridPanel* InventoryGrid = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UTextBlock* InventoryTitle = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TSubclassOf<UInventorySlotWidget> InventorySlotWidgetClass;
	
	virtual void NativeConstruct() override;
	virtual FReply NativeOnKeyDown( const FGeometry& InGeometry, const FKeyEvent& InKeyEvent ) override;

public:

	FOnSlotCreatedSignature OnSlotCreated;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FName InventoryName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FVector2D> GridSlotTranslation;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	ATestProjectCharacter* Character = nullptr;

	UGridPanel* GetInventoryGrid() const;
	UInventorySlotWidget* GetSlotWidget() const;
	void SetInventoryComponent(UInventoryComponent* InInventory);
	UFUNCTION(BlueprintCallable)
	void CreateInventorySlots();
	UFUNCTION(BlueprintCallable)
	void ToggleOpacity(const bool bActivated);
	UFUNCTION(BlueprintCallable)
	void RefreshSlot();
	UFUNCTION(BlueprintCallable)
	void ChangeSlotInfo(const int32 IndexSlot, const FItemsInfo NewInfo);
	UFUNCTION(BlueprintCallable)
	void KeyInputs(FName KeyName);
	UFUNCTION(BlueprintCallable)
	void Transfer(const int32 IndexSlot, const FItemsInfo NewInfo, const FName InventName);
};
