// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FunctionLibrary/Types.h"
#include "Interfaces/InspectInterface.h"
#include "InventoryActionWidget.generated.h"

class AItemInspectViewer;
class UInventorySlotWidget;
class UButton;
/**
 * 
 */
UCLASS()
class TESTPROJECT_API UInventoryActionWidget : public UUserWidget, public IInspectInterface
{
	GENERATED_BODY()

	UPROPERTY()
	ATestProjectCharacter* PlayerCharacter = nullptr;

public:
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UButton* UseButton = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UButton* InfoButton = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UButton* DropButton = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FItemsInfo ItemInfo;
	UPROPERTY()
	UInventorySlotWidget* InventorySlotWidget = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AItemInspectViewer> InspectorClass;
	
protected:

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable)
	void OpenInspector();
	UFUNCTION(BlueprintCallable)
	void UseItem();
	UFUNCTION(BlueprintCallable)
	void DropItem();
};
