// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InspectorWidget.generated.h"

class UTextBlock;
class UScrollBox;
/**
 * 
 */
UCLASS()
class TESTPROJECT_API UInspectorWidget : public UUserWidget
{
	GENERATED_BODY()

	FTimerHandle StartScrollTimerHandle;
	FTimerHandle ScrollTimerHandle;

public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FText ItemDescription;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UScrollBox* ScrollBox = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UTextBlock* ItemDescriptionBlock = nullptr;

protected:
	virtual void NativeConstruct() override;
	void StartScroll();
	void Scroll();
	void StopScroll();
	
};
