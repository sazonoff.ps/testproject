// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FunctionLibrary/Types.h"
#include "InventorySlotWidget.generated.h"


class ATestProjectCharacter;
class UInventoryComponent;
class UDragAndDropWidget;
class UInventoryActionWidget;
class UImage;
class UBorder;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnAddActionsSignature, UWidget*);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnToggleOpacitySignature, bool);
DECLARE_MULTICAST_DELEGATE(FOnRefreshSlotSignature);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnChangeSlotInfoSignature, int32, FItemsInfo);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnTransferItemSignature, int32, FItemsInfo, FName);


/**
 * 
 */
UCLASS()
class TESTPROJECT_API UInventorySlotWidget : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY()
	UInventoryActionWidget* ActionWidget = nullptr;

public:
	
	FOnAddActionsSignature OnAddActions;
	FOnToggleOpacitySignature OnToggleOpacity;
	FOnRefreshSlotSignature OnRefreshSlot;
	FOnChangeSlotInfoSignature OnChangeSlotInfo;
	FOnTransferItemSignature OnTransferItem;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	int32 SlotIndex = 0;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FItemsInfo ItemInfo;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FLinearColor ActivatedBorder;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FLinearColor DeactivatedBorder;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UImage* ImageItem = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta= (BindWidget))
	UBorder* SlotBorder = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UInventoryComponent* CharInventory = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	UInventoryComponent* CrimeBoardInventory = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	ATestProjectCharacter* Character = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UInventoryActionWidget> InventoryActionClass;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TSubclassOf<UDragAndDropWidget> DragAndDropWidgetClass;
	UFUNCTION(BlueprintCallable)
	FSlateBrush GetItemIcon() const;
	UFUNCTION(BlueprintCallable)
	void UpdateOpacity(const bool bActivated);
	UFUNCTION(BlueprintCallable)
	UInventoryActionWidget* GetActionWidget();
	UFUNCTION(BlueprintCallable)
	void SetActionWidget(UInventoryActionWidget* NewActionWidget);
	

protected:
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent,
									  UDragDropOperation*& OutOperation) override;
	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
	                          UDragDropOperation* InOperation) override;
	virtual void NativeOnDragEnter(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent,
	                               UDragDropOperation* InOperation) override;
	virtual  void NativeOnDragLeave(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;
	void ToggleActions();
};
