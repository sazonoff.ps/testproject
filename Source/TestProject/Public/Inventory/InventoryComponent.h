// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FunctionLibrary/Types.h"
#include "InventoryComponent.generated.h"

class ACrimeBoard;
DECLARE_MULTICAST_DELEGATE_OneParam(FOnInventoryOpenSignature, FName);
DECLARE_MULTICAST_DELEGATE(FOnInventoryUpdateSignature);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTPROJECT_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	FOnInventoryOpenSignature OnInventoryOpen;
	FOnInventoryUpdateSignature OnInventoryUpdate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory")
	FName InventoryName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory")
	TArray<FItemsInfo> Inventory;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory")
	int32 SlotsCount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory")
	FVector2D InventoryWidgetPosition = FVector2D(700.0f, 0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Inventory")
	ACrimeBoard* CrimeBoard; 
	
	UInventoryComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void ToggleInventory();
	bool AddToInventory(const FItemsInfo NewItem);
	bool CheckCanTakeItem(int32& OutFreeSlot) const;
	UFUNCTION(BlueprintCallable)
	void RemoveItem(int32 SlotIndex);
	UFUNCTION(BlueprintCallable)
	void TransferItem(const int32 SlotIndex, const FItemsInfo ItemInfo, const FName InventName);
	UFUNCTION(BlueprintCallable)
	void UpdateSlotInfo(const int32 IndexSlot, const FItemsInfo NewInfo);
		
};
