// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FunctionLibrary/Types.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemSettings")
	UDataTable* ItemInfoTable = nullptr;

	UFUNCTION(BlueprintCallable)
	bool GetItemInfoByName(const FName InItemName, FItemsInfo& OutInfo) const;
};
