// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InspectInterface.generated.h"

class ATestProjectCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInspectInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TESTPROJECT_API IInspectInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	
	virtual void Interact(ATestProjectCharacter* Character);
	virtual void Inspect(ATestProjectCharacter* Character, UStaticMesh* Item, FName ItemName, FText ItemDescription,
	                     bool bIsUI, UUserWidget* UI);
};
