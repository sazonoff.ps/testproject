// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FunctionLibrary/Types.h"
#include "GameFramework/Actor.h"
#include "Interfaces/InspectInterface.h"
#include "ItemMaster.generated.h"


class AItemInspectViewer;
class UInteractWidget;
class UWidgetComponent;
class USphereComponent;

UCLASS()
class TESTPROJECT_API AItemMaster : public AActor, public IInspectInterface
{
	GENERATED_BODY()

	UPROPERTY()
	ATestProjectCharacter* PlayerCharacter = nullptr;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USphereComponent* CollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UWidgetComponent* WidgetComponent = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UInteractWidget> InteractWidgetClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<AItemInspectViewer> InspectorClass;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI")
	UUserWidget* InteractWidget = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	FName ItemName;
	FItemsInfo ItemInfo;

public:
	
	AItemMaster();
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(ATestProjectCharacter* Character) override;

protected:
	
	virtual void BeginPlay() override;
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	virtual void NotifyActorEndOverlap(AActor* OtherActor) override;
	void InitItem();
};
