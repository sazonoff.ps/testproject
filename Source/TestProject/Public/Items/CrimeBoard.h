// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemMaster.h"
#include "Interfaces/InspectInterface.h"
#include "CrimeBoard.generated.h"

class UInventoryComponent;
UCLASS()
class TESTPROJECT_API ACrimeBoard : public AItemMaster
{
	GENERATED_BODY()
	
public:	
	
	ACrimeBoard();

	UFUNCTION(BlueprintCallable)
	UInventoryComponent* GetInventoryComponent() const;

protected:

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UInventoryComponent* InventoryComponent;
	
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(ATestProjectCharacter* Character) override;

};
