// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/InspectInterface.h"
#include "ItemInspectViewer.generated.h"

class USpringArmComponent;
class UInspectorWidget;

UCLASS()
class TESTPROJECT_API AItemInspectViewer : public AActor, public IInspectInterface
{
	GENERATED_BODY()

	UPROPERTY()
	ATestProjectCharacter* PlayerCharacter = nullptr;
	bool bRotated = false;
	bool bUseUI = false;
	UPROPERTY()
	UUserWidget* InventoryUI = nullptr;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UStaticMeshComponent* ItemMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USceneCaptureComponent2D* SceneCaptureComponent = nullptr;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Components")
	USpringArmComponent* CameraBoom;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UInspectorWidget> InteractWidgetClass;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "UI")
	UUserWidget* InspectWidget = nullptr;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Camera")
	float ZoomSpeed = 1.0f;

protected:
	
	virtual void BeginPlay() override;
	virtual void Inspect(ATestProjectCharacter* Character, UStaticMesh* Item, FName ItemName, FText ItemDescription,
	                     bool bIsUI, UUserWidget* UI) override;
	void BindToInput();
	void StartInteract();
	void StartRotateItem();
	void StopRotateItem();
	void RotateXAxis(const float Value);
	void RotateYAxis(const float Value);
	void ZoomIn();
	void ZoomOut();
	
public:
	AItemInspectViewer();
	virtual void Tick(float DeltaTime) override;
};